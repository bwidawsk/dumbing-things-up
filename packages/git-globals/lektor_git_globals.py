# -*- coding: utf-8 -*-
import subprocess

from lektor.pluginsystem import Plugin

def get_git_description():
    proc = subprocess.run(['git', 'describe', '--always'], capture_output=True, text=True, check=True)
    return proc.stdout.strip()

class GitGlobalsPlugin(Plugin):
    name = 'Git Globals'
    description = u'Adds a set of interesting git information to the jinja globals.'

    def on_setup_env(self, **extra):
        self.env.jinja_env.globals.update(
                get_git_description=get_git_description
        )
