title: Bad Engineering Managers
---
author: ben
---
pub_date: 2017-03-11
---
Status: draft
---
_discoverable: False
---
body:
I've had no less than 11 managers in my last 14 years (arguably I had 3 more,
but it was such a short period, I won't even count it). I can say they've taught
me a lot about what I need from them to be successful as an individual
contributor, usually by counter-example, and so I'm taking this opportunity to
repay the favor.

So let's back up and talk about what kinds of managers I see, and what they do.
In my experience, there are 3 types of managers: the **people manager**, the
**architect-manager**, and what I call, the **unmanager**.

Intel mostly has people managers. Their primary role is to oversee intangibles
like career development, performance management, relationships with other
groups, "pass downs". They are usually not held accountable to these by the
employee, and their manager often has very high-level metrics to determine
success. The gray space allows poor managers to thrive and great managers to
struggle. A people manager is highly unlikely to be technically superior to
their employee on the domain and scope of work being done. Even junior
employees. I spent a long time hating on people managers until I realized their
value in reducing the overall number of managers - a people manager should be
able to have many more reports than other managers. People managers are
excellent for a senior team with an established track record. If you're a people
manager you should be meeting with your reports regularly, speaking on behalf of
your team to other groups and companies on decisions your leads made, clearly
communicating what the organization and company are doing and translating that
into how your group specifically can influence that and be impacted by it.

The architect-manager is a manager who focuses on define what the team should
work on - judging impact, priority, and scope of the work. They do this with, or
without input from their team. They also have people management
responsibilities. Architect-managers should know at least as much, if not more
than their reports, and they'd rather "do it myself" if they just had the time.
Because of the extra technical requirements, architect managers usually manage
fewer employees. If you're an architect-manager, you should be making strategic
decisions for your team in terms of commitments, prioritization, and general
decision making of the solution your team provides. You can speak knowledgeably
about a majority of what your team is working on. You represent the team in most
aspects externally, but you do not get into the nitty-gritty of any specific
solution. Junior employees like that they can discuss technical issues with you
and senior employees want you to be more of a people manager.

There are two types of unmanagers. The first is one who is forced to manage
against their will due to some organizational change or unwanted "promotion".
They never signed on for this, and they'll be damned if they do more than the
minimum required. They might think they're the architect but don't have the
chops, or they focus entirely on technical issues without guiding their reports.
If you don't interact with your reports much, and you aren't representing your
team in any sort of leadership manner, you're probably an unmanager.

The unmanager is implicitly a bad manager, always. Otherwise, a bad manager can
be a subjective state as well as a transient. What works for one person
obviously doesn't work for another, and perhaps slightly less obvious, what
might work for you at one point in your career might not be optimal at another
point.

1.  Don't surprise me. Even if it was a surprise to you.

    The most obvious here is in regards to your review. Let me lay it out here,
    your report **does not** have the slightest clue what you think about their
    contributions unless you tell them very specifically. Say things like,
    *you're performing to my expectations* or, *you're far surpassing my
    expectations for someone of your experience*, *I expected you to complete
    your project in 3 months based feedback from the team, and it took you 9
    months*. When your report isn't meeting your expectations. Do not say things
    like, *I think you're a great engineer*, and *people respect you*, *you're
    not doing well*. The former gives concrete feedback about what the report
    should expect at review time and leaves them to question how to either
    continue on pace, object to a specific criticism, or try to improve. It's
    not your job to make all your reports perform above expectations, but it is
    your job to let them know how they're doing and offer guidance if they want
    to do more or less. Whether the surprise is a positive, or negative impact
    on the employee, the surprise itself is unwelcome. If you end up saying,
    *Talk to HR*, you've failed.

    The other surprise is in regards to what you'll be doing for the next 6
    months. Sometimes as a manager this isn't in your control, but usually, I'd
    argue even most often, you know what might be coming and preparing your
    employee for it can only help soften the blow of them having to stop working
    on something they love, or to go work on something they loathe. In fact,
    they might even think you're there to help and support **them**. Perish the
    thought...
2.  You're dumb.
3.  We can't be friends. But we're also not enemies
4.  We can't manage our time well.
5.  We don't have good work-life balance.
6.  My job is to make you look good - not do your job for you
